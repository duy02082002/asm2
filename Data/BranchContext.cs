﻿using BranchManagement.Entity;
using Microsoft.EntityFrameworkCore;

namespace BranchManagement.Data
{
    public class BranchContext : DbContext
    {
        public DbSet<Branch> Branches { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                "server =(local); Initial Catalog= CustomerManager;uid=sa;pwd=12345; TrustServerCertificate=True");
        }
    }
}
