﻿using BranchManagement.Data;
using BranchManagement.Entity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BranchManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BranchController : ControllerBase
    {

        BranchContext _context;
        DbSet<Branch> _dbSet;

        public BranchController()
        {
            _context = new BranchContext();
            _dbSet = _context.Set<Branch>();
        }

        [HttpGet]
        public IActionResult getAllBranch()
        {
            List<Branch> branchs = _dbSet.ToList();
            return Ok(branchs);

        }

        [HttpGet("{id}")]
        public IActionResult getBranch(int id)
        {
            Branch branch = _dbSet.ToList().FirstOrDefault(b => b.BranchId == id);
            return Ok(branch);
        }

        [HttpPost]
        public IActionResult createBranch(Branch branch)
        {
            try
            {
                _dbSet.Add(branch);
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPut]
        public IActionResult updateBranch(Branch branch)
        {
            try
            {
                var tracker = _context.Attach(branch);
                tracker.State = EntityState.Modified;
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult deleteBranch(int id)
        {
            try
            {
                Branch branch = _dbSet.ToList().FirstOrDefault(b => b.BranchId == id);
                _dbSet.Remove(branch);
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
