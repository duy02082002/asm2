﻿namespace BranchManagement.Entity
{
    public class Branch
    {
        public int BranchId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }

        public Branch()
        {
        }

        public Branch(int branchId, string name, string address, string city, string state, string zipCode)
        {
            BranchId = branchId;
            Name = name;
            Address = address;
            City = city;
            State = state;
            ZipCode = zipCode;
        }
    }
}
